# De bouwblokken van OPEN DEI Soft Infrastructure bieden een goede categorisering voor de "Digilab-puzzels"

- Status: draft
- Deciders: Digilabteam, FDS Architectenteam
- Date: [YYYY-MM-DD van wanneer deze beslissing het laatst is bijgewerkt] <!-- optional. To customize the ordering without relying on Git creation dates and filenames -->
- Tags: [comma separated lijst van tags] <!-- optional -->

Issue: [description | ticket/issue URL] <!-- optional -->

## Context en probleemstelling

In het Digilab is door de tijd heen een lijst verzameld van zogenaamde 'puzzels', de befaamde 'puzzellijst'.
Dit zijn vraagstukken waar 'ooit' eens antwoord voor moet komen voor een werkend Federatief Datastelsel. Deze vraagstukken zijn van heel verschillend niveau en aspect.
Welke indeling is voor Digilab en het Nederlandse datastelsel de beste (of een goed begin)?

## Beslissingsfactoren <!-- optional -->

- [Digilab](https://digilab.overheid.nl/)
- [Realisatie InterBestuurlijke DataStrategie (IBDS)](https://realisatieibds.pleio.nl/) en het 
- [Federatief DataStelsel (FDS)](https://realisatieibds.pleio.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
- [Regie op Gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/)
- [Common Ground](https://commonground.nl/)
- [Data bij de Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/)
- … <!-- numbers of drivers can vary -->

## Overwogen opties

- [Puzzellijst categorieën](#puzzellijst-categorieën)
- [OPEN DEI Soft infrastructure](#open-dei-soft-infrastructure)
- [API Referentiearchitectuur](#api-referentiearchitectuur) (ARA Birdseye View)
- … <!-- numbers of options can vary -->

## Besluit

Gekozen oplossing: "[OPEN DEI Soft infrastructure](#open-dei-soft-infrastructure)" met toevoeging van categorie 'Onderzoek', omdat dit een veelgebruikt model is voor datastelsels (data spaces) en herkenbare onderwerpen beschrijft. De toevoeging van 'Onderzoek' is relevant voor zaken die nog niet precies in een onderwerp passen.

Onderwerpen zijn dan (NLse vertalingen):

- Interoperabiliteit
  - Datamodellen & formaten
  - API's
  - Herleidbaarheid & traceerbaarheid
- Vertrouwen
  - Identity management
  - Access & usage control / policies
  - Betrouwbare uitwisseling
- Waarde van data
  - Metadata & discovery-protocol
  - Verantwoording datagebruik
  - Publicatie & Marktplaats-services
- Governance
  - Overkoepelende samenwerkingsovereenkomst
  - Operationeel
  - Continuïteitsmodel
- Onderzoek _(is dit een 'hoofdklasse' of komt deze per hoofdklasse voor?)_

### Positieve gevolgen <!-- optional -->

- [bijv. verbetering van de kwaliteit, opvolgende besluiten benodigd, ...]
- …

### Negatieve Consequences <!-- optional -->

- [bijv. compromis op kwaliteit, opvolgende besluiten benodigd, ...]
- …

## Pros en Cons van de oplossingen <!-- optional -->

### Puzzellijst categorieën

De initiële categorisering van de Digilab-puzzels was:

- API capabilities
- API integratie
- Connecties
- Datakwaliteit
- Infrastructuur
- Gegevensbescherming
- Informatiebeveiliging
- Financiëel
- Juridisch
- Compliancy
- Research
- Governance
- Logging

**Voor- en nadelen**

- Goed, want het benoemt bekende thema's.
- Slecht, omdat de thema's van heel verschillend niveau zijn.
- Slecht, omdat thema's overlappen.
- … <!-- numbers of pros and cons can vary -->

### OPEN DEI Soft Infrastructure

De [OPEN DEI](opendei.eu) Task Force 1 heeft een [Design Principles for Data Spaces](https://design-principles-for-data-spaces.org/) opgesteld.
Hierin wordt een capability model geïntroduceerd van 'Soft Infrastructure':

![OPEN DEI Soft Infrastructure](../images/opendei-soft-infrastructure.png)

```mermaid
C4Context
      title OPEN DEI Soft Infrastructure
      Enterprise_Boundary(si0, "Soft infrastructure") {
        System_Boundary(si1, "Technical Building Blocks") {
        System_Boundary(i0, "INTEROPERABILITY") {
            System(i1, "Data Models & Formats")
            System(i2, "Data Exchange APIs")
            System(i3, "Provenance and tracebility")
        }
        System_Boundary(t0, "TRUST") {
            System(t1, "Identity management")
            System(t2, "Access & usage control / policies")
            System(t3, "Trusted Exchange")
        }
        System_Boundary(d0, "DATA VALUE") {
            System(d1, "Metadata & Discovery Protocol")
            System(d2, "Data Usage Accounting")
            System(d3, "Publication & Marketplace Services")
        }
        }
        System_Boundary(si2, "Governance Building Blocks") {
        System_Boundary(g0, "GOVERNANCE") {
            System(g1, "Overarching cooperation agreement")
            System(g2, "Operational (e.g. SLA)")
            System(g3, "Continuity Model")
        }
        }
      }
    %%   UpdateElementStyle(si0, $fontColor="white", $bgColor="blue", $borderColor="blue")

      UpdateElementStyle(i1, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(i2, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(i3, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(t1, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(t2, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(t3, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(d1, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(d2, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(d3, $fontColor="white", $bgColor="orange", $borderColor="orange")
      UpdateElementStyle(g1, $fontColor="white", $bgColor="green", $borderColor="green")
      UpdateElementStyle(g2, $fontColor="white", $bgColor="green", $borderColor="green")
      UpdateElementStyle(g3, $fontColor="white", $bgColor="green", $borderColor="green")

      UpdateLayoutConfig($c4ShapeInRow="1", $c4BoundaryInRow="4")
```

**Voor- en nadelen**

- Goed, omdat dit model breed gebruikt wordt om data space ontwikkelingen met elkaar te vergelijken, bijv. deze [Geonovum Verkenning Dataspaces](https://docs.geostandaarden.nl/eu/VerkenningDataspaces/)
- Goed, omdat FDS ook dit model gebruikt voor de uitwerking van de 'FDS capabilities' (to be)
- Goed, omdat de thema's herkenbaar zijn en enigszins overeenkomstig met de [Puzzellijst categorieën](#puzzellijst-categorieën)
- Goed, omdat het document [Design Principles for Data Spaces](https://design-principles-for-data-spaces.org/) een heel bruikbaar en goed document is (pas op: mening van beslissers :smile: )
- Slecht, omdat ... tja, geen idee eigenlijk
- … <!-- numbers of pros and cons can vary -->

### API Referentiearchitectuur

Met de [API Referentiearchitectuur (ARA)](https://commonground.nl/blog/view/0c4b6bf9-9829-479b-985a-4ccf1696ed66/op-naar-een-api-referentiearchitectuur-ara-timmeren-we-nog-aan-de-juiste-weg) wordt getracht om de ontwikkelingen van (oa) VNG tegen het licht te houden.
Vanuit een 'ARA Birdseye View' stelt Common Ground zich de vraag of we nog steeds op de goede weg zitten.
Terecht vraag. Goede discussie. Goede input.

**Voor- en nadelen**

- Goed, omdat kritische vragen stelt over API's, gebruik van API's, opzet van API's, alles rondom API's.
- Slecht, omdat ook vooral beperkt is tot API's; het grotere geheel ontbreekt wat voor de Digilab puzzels en bredere FDS architectuur wel beoogd is.
- … <!-- numbers of pros and cons can vary -->

## Links <!-- optional -->

- [Link naam](link to adr) <!-- example: Refined by [xxx](yyyymmdd-xxx.md) -->
- … <!-- numbers of links can vary -->
