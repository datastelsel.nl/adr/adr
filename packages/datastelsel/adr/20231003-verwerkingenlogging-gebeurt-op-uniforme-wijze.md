# Verwerkingenlogging gebeurt op uniforme wijze

- Status: draft
- Deciders: Eelco
- Date: 2023-10-03
- Tags: AVG

## Context en probleemstelling

Organisaties die persoonsgegevens verwerken zijn conform de Algemene Verordening Gegevensbescherming (AVG) en de Uitvoeringswet AVG verplicht om aan te kunnen tonen dat een verwerking van persoonsgegevens aan de belangrijkste beginselen van verwerking voldoet, zoals rechtmatigheid, transparantie, doelbinding en juistheid.

Om aan deze verantwoordingsplicht te kunnen voldoen is het van belang dat per verwerking de belangrijkste metagegevens van de verwerkingen worden vastgelegd. Dit gebeurt momenteel nauwelijks, en als het gebeurt dan is dat op zeer uiteenlopende manieren, waarvan een groot deel niet voldoet aan het wettelijk kader. Daarnaast volgen vanuit diverse beleidsdoelstellingen eisen aan deze logging waar nu geen invulling aan wordt gegeven.


## Beslissingsfactoren

- Wettelijk kader:
  - [Algemene verordening gegevensbescherming (AVG, GDPR)](https://gdpr-text.com/nl/)
  - [Uitvoeringswet Algemene verordening gegevensbescherming](https://wetten.overheid.nl/BWBR0040940/2021-07-01/0)

- Context:
  - Doelstellingen [Regie op Gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/)
  - Doelstellingen [Realisatie InterBestuurlijke DataStrategie (IBDS)](https://realisatieibds.pleio.nl/)
  - Doelstellingen [Federatief DataStelsel (FDS)](https://realisatieibds.pleio.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
  - Doelstellingen [Common Ground](https://commonground.nl/)
  - Doelstellingen [Data bij de Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/)
  - Doelstellingen [Digilab](https://digilab.overheid.nl/)

- Architecture Decision Records:
  - x

- OpenDEI:
  - Interoperabiliteit
    - API's
  - Waarde van Data
    - Verantwoording Datagebruik
  - Governance
    - Operationeel


## Overwogen opties

1. Alle organisaties houden logs van verwerkingen bij binnen de kaders van de wet, zonder standaardisatie

2. Alle organisaties houden logs van verwerkingen bij volgens een standaard:
  1. [Standaard Verwerkingenlogging](https://vng-realisatie.github.io/gemma-verwerkingenlogging/) zoals opgesteld door de Vereniging Nederlandse Gemeenten
  2. Standaard Verwerkingenlogging zoals doorontwikkeld op bovenstaaande door MinBZK

3. ...


## Besluit

Gekozen oplossing: Optie 2.2, omdat alleen met deze standaard voor Verwerkingenlogging invulling kan worden gegeven aan de eisen en wensen die volgen uit de beslissingsfactoren.


### Positieve gevolgen <!-- optional -->

- [bijv. verbetering van de kwaliteit, opvolgende besluiten benodigd, ...]
- …

### Negatieve Consequences <!-- optional -->

- [bijv. compromis op kwaliteit, opvolgende besluiten benodigd, ...]
- …

## Pros en Cons van de oplossingen <!-- optional -->

### [optie 1]

[voorbeeld | beschrijving | verwijzen naar meer informatie | …] <!-- optional -->

- Goed, omdat [argument a]
- Goed, omdat [argument b]
- Slecht, omdat [argument c]
- … <!-- numbers of pros and cons can vary -->

### [optie 2]

[voorbeeld | beschrijving | verwijzen naar meer informatie | …] <!-- optional -->

- Goed, omdat [argument a]
- Goed, omdat [argument b]
- Slecht, omdat [argument c]
- … <!-- numbers of pros and cons can vary -->

### [optie 3]

[voorbeeld | beschrijving | verwijzen naar meer informatie | …] <!-- optional -->

- Goed, omdat [argument a]
- Goed, omdat [argument b]
- Slecht, omdat [argument c]
- … <!-- numbers of pros and cons can vary -->


## Links <!-- optional -->

- [Link naam](link to adr) <!-- example: Refined by [xxx](yyyymmdd-xxx.md) -->
- … <!-- numbers of links can vary -->
